const { i18n } = require("./next-i18next.config");
const withPWA = require("next-pwa");
// const runtimeCaching = require("next-pwa/cache");

module.exports = withPWA({
  reactStrictMode: true,
  pwa: {
    disable: process.env.NODE_ENV === "development",
    dest: "public",
    // runtimeCaching,
  },
  i18n,
  // typescript: {
  //   ignoreBuildErrors: true,
  // },
  // images: {
  //   loader: 'default',
  //   domains: [process.env.NEXT_PUBLIC_IMAGE_SERVER_URL || ''],
  // },
});
