import { NextSeo } from "next-seo";
import { PUBLIC_SITE_URL } from "../../constants/constant";

const Seo = ({ title, description, path }) => {
  return (
    <NextSeo
      title={title}
      description={description}
      openGraph={{
        url: `${PUBLIC_SITE_URL}/${path}`,
        title,
        description,
        images: [
          {
            url: "/images/beehive-logo.png",
            width: 800,
            height: 600,
            alt: "Logo",
          },
          {
            url: "/images/beehive-logo.png",
            width: 900,
            height: 800,
            alt: "Logo",
          },
        ],
      }}
    />
  );
};

export default Seo;
