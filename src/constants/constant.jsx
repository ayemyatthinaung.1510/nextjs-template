const { NEXT_PUBLIC_SITE_URL } = process.env;

export const PUBLIC_SITE_URL = NEXT_PUBLIC_SITE_URL;
