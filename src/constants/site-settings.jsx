import { USFlag } from "../../public/images/language/USFlag";
import { MMFlag } from "../../public/images/language/MMFlag";

export default siteSettings = {
  name: "Site Name",
  description: "Fastest Website",
  author: {
    name: "Aye Myat Thin Aung",
    // websiteUrl: "https://redq.io",
    address: "Yangon, Myanmar",
  },
  logo: {
    url: "/assets/images/header-logo.png",
    alt: "Logo",
    href: "/",
    width: 160,
    // height: 120,
    height: 60,
  },
  currencyCode: "MMK",
  site_header: {
    menu: [
      {
        id: 2,
        path: "/restaurants/",
        label: "menu-foods",
      },
    ],
    defaultLanguage: "en",
    languageMenu: [
      {
        id: "en",
        name: "english",
        value: "en",
        icon: <USFlag />,
      },

      {
        id: "my",
        name: "myanmar",
        value: "my",
        icon: <MMFlag />,
      },
    ],
  },
};
