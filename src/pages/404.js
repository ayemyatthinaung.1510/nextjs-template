// import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const NotFoundPage = () => (
  <div className="h-screen flex justify-center items-center w-full ">
    <div>
      <img
        src="https://tailwindui.com/img/logos/workflow-mark-blue-600.svg"
        className="mx-auto h-12 w-auto"
        alt="Wal Yaung Estate 404 error page"
      />
      <div className="text-center mt-10">
        <h4 className="text-indigo-600 font-bold">404 ERROR</h4>
        <h1 className="text-black-900 font-bold text-5xl py-3">
          Page not found
        </h1>
        <p className="text-gray-400">
          Sorry, we could not find the page you are looking for.
        </p>
        <div className="text-indigo-600 font-bold mt-4">
          <a href="/">
            Go back home&nbsp;
            {/* <FontAwesomeIcon icon={faArrowRight} /> */}
          </a>
        </div>
      </div>
    </div>
  </div>
);

export default NotFoundPage;
