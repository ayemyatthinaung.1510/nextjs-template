import React from "react";
import Seo from "../components/seo/seo";
const Home = () => (
  <>
    <Seo
      title="Bee Delivery"
      description="Fastest website built with React, NextJS, TypeScript, React-Query and Tailwind CSS."
      path="/"
    />
    <div>
      Home
      <div>Hello</div>
    </div>
  </>
);

export default Home;
