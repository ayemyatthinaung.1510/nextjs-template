import Document, { Html, Main, NextScript, Head } from "next/document";

export default class MyDocument extends Document {
  render() {
    return (
      <Html lang="en">
        <Head>
          {/* <meta charSet="utf-8" />
          <meta name="keywords" content={KEYWORDS} />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge,chrome=1" />
          <meta name="geo.placename" content={LOCATION.address} />
          <meta name="geo.region" content="MM-06" />
          <meta
            name="geo.position"
            content={`${LOCATION.lat};${LOCATION.lng}`}
          />
          <meta name="ICBM" content={`${LOCATION.lat},${LOCATION.lng}`} />
          <meta property="article:publisher" content={FACEBOOK_URL} />
          <meta property="og:site_name" content={TITLE} />
          <meta property="og:type" content="website" />
          <meta property="og:image" content={OG_IMAGE} /> */}

          <link
            rel="stylesheet"
            href="https://mmwebfonts.comquas.com/fonts/?font=pyidaungsu"
          />
          <link
            rel="stylesheet"
            href="https://mmwebfonts.comquas.com/fonts/?font=zawgyi"
          />
        </Head>
        <body id="root">
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
