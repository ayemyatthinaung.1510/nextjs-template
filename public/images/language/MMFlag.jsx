export const MMFlag = ({ ...rest }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="23"
      height="17"
      viewBox="0 0 18 12"
    >
      <rect width="18" height="12" fill="#FECB00" />
      <rect width="18" height="8" y="4" fill="#34B233" />
      <rect width="18" height="4" y="8" fill="#EA2839" />
      <g transform="translate(9,6.422) scale(4.422)">
        <polygon
          id="pt"
          points="-0.3249196962329062,0 0,-1 0.3249196962329062,0"
          fill="#FFF"
        />
        <use xlinkHref="#pt" transform="rotate(-144)" />
        <use xlinkHref="#pt" transform="rotate(-72)" />
        <use xlinkHref="#pt" transform="rotate(72)" />
        <use xlinkHref="#pt" transform="rotate(144)" />
      </g>
    </svg>
  );
};
